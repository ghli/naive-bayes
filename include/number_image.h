#ifndef _NUMBER_IMAGE_THING_
#define _NUMBER_IMAGE_THING_
#include <string>
#include <vector>
#include <iostream>
//template<int W, int H>
class NumberImage {
    public:
        int nCol_;
        int nRow_;
        std::vector<char> data_;
        // The proper label of the number image.
        int label;
        // Creates an 2d image with dimensions.
        NumberImage(int, int);
        // Populates the image's data with a stream.
        friend std::istream& operator>>(std::istream&, NumberImage&);
        // Processes the data into prescence or non-prescence of features.
        std::vector<bool> process() const;
};
#endif
