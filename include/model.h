#ifndef _MODEL_THING_
#define _MODEL_THING_
#include <vector>
#include <string>
#include <fstream>
#include <map>
#include "number_image.h"

static const double kLaplaceSmoothing = .1;
class Model {
    //private:
    public:
        int rows_;
        int cols_;
        int classes_;
        // Counts how many features have occured for a certain class.
        std::map<int, std::vector<long>> feature_freq_;
        // Counts the total number of class appearances.
        std::map<int, int> class_freq_;
        // Holds P(f|c).
        std::map<int, std::vector<double>> feature_prob_; 
        // Holds P(c).
        std::map<int, double> class_prob_;
        // False if we haven't generated probabilities,
        // Which means we can't predict anything.
        bool generated_ = false;
    //public:
        // A model with classes, rows, and cols.
        Model(int, int, int);
        // Generate probabilities.
        void generate();
        // Generate the respective probabilities of an image being in the classes.
        std::vector<double> predict_probs(const NumberImage&) const;
        // Predict the class of the image.
        // Essentially finding the index of the max in the vector returned by\
        // predict_probs().
        int predict(const NumberImage&) const;
        // Add an image for training.
        // The image must have the proper label member data initialized,
        // Or the model will be off.
        void add(const NumberImage&); 
        // Adds an image for training with the correct label.
        void add(int, const NumberImage&);
        // Loads a file containing images and a file containing the corresponding labels.
        // The filenames must exist for the load to work.
        void load_training_files(const std::string&, const std::string&);
        // Evaluate a file containing images and a file containing the corresponding labels.
        std::string evaluate_testfile(const std::string&, const std::string&);
        // Saves the probabilities in this model.
        // The model must have been generated, otherwise there's not point in saving.
        bool save_prob_file(const std::string&);
        // Loads probabilities from a file.
        // Files saved from models of different parameters will load,
        // But the behaviour will be undefined.
        bool load_prob_file(const std::string&);
};
#endif
