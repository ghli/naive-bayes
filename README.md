# Naive Bayes Classifier 
## Usage
```
naivebayes classes rows columns -t training_images training_labels test_filename test_labelfilename | 
naivebayes classes rows columns -t training_images training_labels -s save_name |
naivebayes classes rows columns -l saved_model_file test_file test_labelfile
```

