#define CATCH_CONFIG_MAIN
#include <fstream>
#include <vector>
#include "catch.hpp"
#include "number_image.h"

TEST_CASE("Read and Process Test") {
    NumberImage img(2,2);
    std::ifstream test_in("data/testimg");
    test_in >> img; 
    std::vector<bool> correct_vec;
    correct_vec.push_back(true);
    correct_vec.push_back(false);
    correct_vec.push_back(false);
    correct_vec.push_back(true);
    REQUIRE(correct_vec == img.process());
}

