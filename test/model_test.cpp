#define CATCH_CONFIG_MAIN
#include <fstream>
#include <vector>
#include "catch.hpp"
#include "model.h"

TEST_CASE("Model Basic Tests") {
    std::ifstream input("data/testimg");
    NumberImage img(2,2);
    input >> img;
    Model test_model(2, 2, 2);
    SECTION("Add Test") {
        test_model.add(0, img);
        std::vector<long> correct_vector;
        correct_vector.push_back(1);
        correct_vector.push_back(0);
        correct_vector.push_back(0);
        correct_vector.push_back(1);
        REQUIRE(correct_vector == test_model.feature_freq_[0]);
    }
    
    SECTION("Probability Test") {
        test_model.add(0, img);
        input >> img;
       test_model.add(0, img);
        test_model.generate();

        std::vector<double> correct_vector;
        correct_vector.push_back((kLaplaceSmoothing + 1)/(2*kLaplaceSmoothing + 2));
        correct_vector.push_back((kLaplaceSmoothing)/(2*kLaplaceSmoothing + 2));
        correct_vector.push_back((kLaplaceSmoothing)/(2*kLaplaceSmoothing + 2));
        correct_vector.push_back((kLaplaceSmoothing + 1)/(2*kLaplaceSmoothing + 2));

        REQUIRE(correct_vector == test_model.feature_prob_[0]);
    }

    SECTION("Single Prediction Test") {
        test_model.add(0, img);
        test_model.add(0, img);
        test_model.add(0, img);
        test_model.add(0, img);

        input >> img;
        input >> img;
        test_model.add(1, img);
        test_model.add(1, img);
        test_model.add(1, img);
        test_model.add(1, img);

        test_model.generate();
        
        REQUIRE(1 == test_model.predict(img));
    }
}

TEST_CASE("Aggregate Data Tests") {
    std::ifstream trainingimg_input("data/trainingimages");
    std::ifstream traininglabel_input("data/traininglabels");

    std::ifstream testimg_input("data/testimages");
    std::ifstream testlabel_input("data/testlabels");

    Model test_model(10, 28, 28);

    NumberImage img(28,28);
    int label;

    while (trainingimg_input >> img && traininglabel_input >> label)
        test_model.add(label, img);

    test_model.generate();

    SECTION("Accuracy Test") {
        int correct = 0;
        int total = 0;
        
        while (testimg_input >> img && testlabel_input >> label) {
            ++total;
            if (test_model.predict(img) == label)
                ++correct;
        }
    
        // Random chance is .1, but we're much better than random.
        REQUIRE((double)correct/total > .5);
    }

    SECTION("Save and Load Test") {
        // Make sure a write is ok.
        CHECK(test_model.save_prob_file("output") != false);
        
        Model new_model(10, 28, 28);
        // Make sure the read is fine.
        CHECK(new_model.load_prob_file("output") != false);

        CHECK(test_model.feature_prob_ == new_model.feature_prob_);
        CHECK(test_model.class_prob_ == new_model.class_prob_);
    }
}
