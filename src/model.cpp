#include "number_image.h"
#include "model.h"
#include <cmath>
#include <algorithm>

Model::Model(int class_num, int row_num, int col_num) : classes_(class_num),
                                                    rows_(row_num),
                                                    cols_(col_num) {
    for (int i = 0; i < class_num; ++i) {
        feature_freq_[i] = std::vector<long>(row_num*col_num, 0);
        class_freq_[i] = 0;
        feature_prob_[i] = std::vector<double>(row_num*col_num, 0);
        class_prob_[i] = 0; 
    }
}
void Model::generate() {
    long total_images = 0;
    for (int c = 0; c < class_freq_.size(); ++c) {
        total_images += class_freq_[c];
    }
    // Calculate frequency of every feature of every class.
    for (int c = 0; c < class_freq_.size(); ++c) {
        for (int i = 0; i < feature_freq_[c].size(); ++i) {
            feature_prob_[c][i] = 
                (kLaplaceSmoothing + feature_freq_[c][i])
                /(2*kLaplaceSmoothing + class_freq_[c]);
        }
        // Calculate P(class).
        class_prob_[c] = (double)class_freq_[c]/total_images;
    }
    generated_ = true;
}

void Model::add(int image_class, const NumberImage& img) {
    std::vector<bool> feature_prescence = img.process();
    // Adds count for every present feature.
    for (int i = 0; i < feature_prescence.size(); ++i) {
        if (feature_prescence[i]) {
            feature_freq_[image_class][i] += 1;
        }
    }
    class_freq_[image_class] += 1;
}

void Model::add(const NumberImage& img) {
    add(img.label, img);
}
std::vector<double> Model::predict_probs(const NumberImage& img) const {
    std::vector<double> posterior_probabilities(class_freq_.size(), 0);
    // If we haven't generated the predictive model, we can't predict anything.
    //if (!generated_)
     //   return posterior_probabilities;

    auto feature_prescence = img.process();
    for (int c = 0; c < class_prob_.size(); ++c) {
        posterior_probabilities[c] = log(class_prob_.find(c)->second);
        for (int i = 0; i < feature_prescence.size(); ++i) {
            // If it has a feature, we want the probability.
            // Else, we want the probability of it not being there i.e. 1-P
            if (feature_prescence[i])
                posterior_probabilities[c] += log(feature_prob_.find(c)->second[i]); 
            else
                posterior_probabilities[c] += log(1-feature_prob_.find(c)->second[i]);
        }
        posterior_probabilities[c] = posterior_probabilities[c];
    }
    return posterior_probabilities;
}

int Model::predict(const NumberImage& img) const {
    auto probabilities = predict_probs(img);
    // The distance from the beginning is the class it is
    // E.g {0: .1 1: .2 2:.7}
    // Max element is the iterator begin() + 2;
    return std::distance(probabilities.begin(),
            std::max_element(probabilities.begin(),
                probabilities.end()));
}

void Model::load_training_files(const std::string &imagefile,
        const std::string &labelfile) {
    std::ifstream images(imagefile);
    std::ifstream labels(labelfile);

    NumberImage img(rows_, cols_);
    int label;
    // Adds all the training images and generates a model.
    while (images >> img && labels >> label)
        add(label, img);
    generate();
}

std::string Model::evaluate_testfile(const std::string& imagefile,
        const std::string &labelfile) {
    std::ifstream images(imagefile);
    std::ifstream labels(labelfile);
    
    NumberImage img(rows_, cols_);
    int label;

    // Makes 2d vectors to hold the count and percentage of confusion.
    auto confusion_matrix_count = std::vector<std::vector<int>>(classes_,
                                             std::vector<int>(classes_, 0));
    auto confusion_matrix = std::vector<std::vector<double>>(classes_,
                                        std::vector<double>(classes_, 0));
    int class_count[classes_] = {0};

    // Evaluates the test files and puts them into the confusion matrix.
    while (images >> img && labels >> label) {
        confusion_matrix_count[label][predict(img)] += 1;
        class_count[label] += 1;
    }

    // Evaluate the occurances by percentage.
    for (int i = 0; i < classes_; ++i) {
        for (int j = 0; j < classes_; ++j) {
            confusion_matrix[i][j] = (double)confusion_matrix_count[i][j]/
                                class_count[i];
        }
    }
    
    // buffer that holds the formatted output string for output.
    char percent_c_str[10];
    std::string result = "";
    for (int i = 0; i < classes_; ++i) {
        for (int j = 0; j < classes_; ++j) {
            // We multiply by 100 because we want percentages.
            snprintf(percent_c_str, 10, "%4.1f ", 100*confusion_matrix[i][j]);
            result.append(std::string(percent_c_str)); 
        }
        result.append("\n");
    }

    return result;
}

bool Model::save_prob_file(const std::string &filename) {
    std::ofstream output(filename);
    output.precision(20);
    if (!output.is_open())
        return false;

    // If we haven't generated a probability, there's no point in saving.
    if (!generated_)
        return false;

    // Save class probabilitier.
    for (auto freq : class_prob_) {
        output << freq.second << std::endl;
    }

    output << std::endl;
    // Save the features for each class.
    for (auto class_probs : feature_prob_) {
        output << class_probs.first << std::endl;
        for (double prob : class_probs.second) {
            output << prob << std::endl;
        }
        output << std::endl;
    }
}

bool Model::load_prob_file(const std::string &filename) {
    std::ifstream input(filename);
    // Clear the probabilities just in case.
    for (auto &probs : feature_prob_)
        probs.second.erase(probs.second.begin(),
                probs.second.end());

    std::string line;
    int old_size = class_prob_.size();
    double confusion_matrix[classes_][classes_] = {0};

    // Read class probabilities.
    int class_num = 0;
    while (getline(input, line)) {
        if (line != "")
            class_prob_[class_num++] = atof(line.c_str());
        else
            break;
    }

    // If we have more classes_ than specified, we have a problem.
    if (old_size != class_prob_.size())
        return false;

    // Read feature probabilities.
    int current_class;
    // Loops through the classes_ of features in the file.
    while (getline(input, line)) {
        current_class = atoi(line.c_str());
        // Loop through the individual features for a class.
        while(getline(input, line)) {
            if (line != "")
                feature_prob_[current_class].push_back(atof(line.c_str()));
            else
                break;
        }
    }
    generated_ = true;
    return true;

}
