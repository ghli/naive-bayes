#include "number_image.h"
#include "model.h"
#include <fstream>
#include <cstdio>
#include <algorithm>

int main(int argc, char **argv) {
    // If user did not supply any arguments.
    if (argc == 1) {
        std::cout << "Usage:\n"
            << "naivebayes classes rows columns -t training_images training_labels test_filename test_labelfilename\n"
            << "naivebayes classes rows columns -t training_images training_labels -s save_name\n"
            << "naivebayes classes rows columns -l saved_model_file test_file test_labelfile\n"
            << std::endl;
        return 0;
    }

    std::string name = argv[0];
    std::vector<std::string> all_args;
    all_args.assign(argv + 1, argv + argc);

    if (all_args.size() != 8 || all_args.size() != 7) {
        std::cout << "Syntax not recognized" << std::endl;
    }
    int classes = atoi(all_args[0].c_str());
    int rows = atoi(all_args[1].c_str());
    int cols = atoi(all_args[2].c_str());

    Model model(classes, rows, cols);
    if (all_args[3] == "-t") {
        model.load_training_files(all_args[4], all_args[5]);
        if (all_args[6] == "-s") {
            model.save_prob_file(all_args[7]);
        } else {
            std::cout << model.evaluate_testfile(all_args[6], all_args[7]);
        }
    } else if (all_args[3] == "-l") {
        model.load_prob_file(all_args[4]);
        std::cout << model.evaluate_testfile(all_args[5], all_args[6]);
    } else {
        std::cout << "Syntax not recognized" << std::endl;
    }
}
