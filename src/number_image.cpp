#include "number_image.h"
#include <iostream>
#include <string>
#include <cstring>

NumberImage::NumberImage(int col, int row) : nCol_(col), nRow_(row) {
}

std::istream& operator>>(std::istream &is, NumberImage &image) {
    std::string line;
    image.data_.erase(image.data_.begin(), image.data_.end());
    for (int i = 0; i < image.nRow_; ++i) {
        std::getline(is, line);
        std::copy(line.begin(), line.end(), std::back_inserter(image.data_));
    } 
    return is;
}

std::vector<bool> NumberImage::process() const {
    std::vector<bool> feature_occurance;
    for (char feature : data_) {
        // If the data there is not a space, it IS a feature.
        feature_occurance.push_back(feature != ' ');
    }
    
    return feature_occurance;
}
